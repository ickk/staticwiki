StaticWiki
==========

Motivation:


Goals:
- Create a single (or few) user wiki site, that is more performant than bare wikimedia for small sites.
- Achievable through use of static site generation.
- Access to raw text files for each page, ie markdown or latex source.
- See just how much of the featureset can be delivered without javascript.

Todo:
- Lots
- Implement authentication.
- Research perf cost of jinja2 template vs simple manual static site gen with regex and string replace. Choose one.
- Consider moving to a real VCS.
- Implement latex support.
- allow offloading of static page delivery to nginx.

Usage:

to get started:
```
git clone --recurse-submodules https://gitlab.com/nickk/staticwiki.git && cd staticwiki
python3 -m venv venv && source venv/bin/activate
pip install -U pip && pip install -r requirements.txt
./init.py
flask run
```