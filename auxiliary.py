import json, string, random
from slugify import slugify

MIN_SLUG_LEN = 3

def ensure_slug(slug):
	if slug == slugify(slug) and len(slug) >= MIN_SLUG_LEN:
		return slug
	else:
		raise ValueError

def get_doc_meta(slug, version=None):
	with open("public/pages/"+slug+".meta", "r") as json_data:
		return json.load(json_data)

def random_identifier(length=6, chars=string.ascii_uppercase+string.ascii_lowercase+string.digits):
	# ! does not return a cryptographically secure value.
	return "".join(random.choice(chars) for _ in range(length))

def render_simple_template(fout, template, file_meta):
	with open(fout, "wb") as fout:
		with open(template, "r") as fin:
			for line in fin:
				for src, target in file_meta.items():
					line = line.replace("{{"+str(src)+"}}", str(target))
					fout.write(line.encode("utf-8"))