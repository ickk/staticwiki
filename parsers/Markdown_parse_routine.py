import markdown
#import shutil

# The parser
class Markdown_parse_routine():
	def __init__(self):
		extension_configs = {
			"codehilite": {
				"linenums": None,
				"guess_lang": False,
				"css_class": "code"
			}
		}
		self.md = markdown.Markdown(extensions=["fenced_code", "codehilite"], extension_configs=extension_configs)

	def parse_file(self, fileIn, fileOut, fileMeta={}, fileHeader=None, fileFooter=None):
		with open(fileOut, "wb") as fout:
			# compute and write header
			if fileHeader:
				with open(fileHeader, "r") as fhead:
					for line in fhead:
						for src, target in fileMeta.items():
							line = line.replace("{{"+str(src)+"}}", str(target))
						fout.write(line.encode("utf-8"))
				fout.write(b'\n')
			# parse and write the document body
			self.md.convertFile(input=fileIn, output=fout)
			self.md.reset()
			# compute and write footer
			if fileFooter:
				fout.write(b'\n')
				with open(fileFooter, "r") as ffoot:
					#shutil.copyfileobj(ffoot, fout, 1024*1024*10)
					for line in ffoot:
						for src, target in fileMeta.items():
							line = line.replace("{{"+str(src)+"}}", str(target))
						fout.write(line.encode("utf-8"))

	def parse_filelist(self, fileList, fileHeader=None, fileFooter=None):
		# fileList should be a list of tuples of the form (fileIn, fileOut)
		for fileIn, fileOut, meta in fileList:
			self.parse_file(fileIn, fileOut, fileMeta=meta, fileHeader=fileHeader, fileFooter=fileFooter)

