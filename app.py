#!/usr/bin/env python3
from flask import Flask, url_for, request, abort, render_template, send_from_directory, make_response, session, redirect
import time, json, os, sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'simplevc'))
from simplevc import SimpleVC
from parsers.Markdown_parse_routine import Markdown_parse_routine
from auxiliary import *

working_dir = os.getcwd()
app = Flask(__name__, template_folder="public")
svc = SimpleVC(working_dir)
md = Markdown_parse_routine()

# if template files change, use new versions instead of those in cache
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config["doc-types"] = {
	"Markdown": "text/markdown; charset=utf-8",
	"LaTeX": "text/latex; charset=utf-8"
}
# max age of form_ids in seconds
app.config["form_id_max_age"] = 30*60
# generate inverse map and list of doc-types
app.config["inv-doc-types"] = {v: k for k, v in app.config["doc-types"].items()}
app.config["list-doc-types"] = list(app.config["doc-types"].keys())
# dictionary of stored form_ids
app.config["previews"] = {}

## receive - create page
@app.route("/<slug>", methods=["POST"]) #TODO permit authorised users only
def app_create_page_post(slug):
	print(request.form)
	try: 
		slug = ensure_slug(slug)
	except ValueError:
		abort(400)

## receive - create collection
@app.route("/<slug>/", methods=["POST"]) #TODO permit authorised users only
def app_create_collection_post(slug):
	print(request.form) # check for "create-collection" = "create-collection"
	return "create collection: " + str(slug)

## manage
@app.route("/w/<slug>", methods=["GET"]) #TODO permit authorised users only
def app_w_get(slug):
	print("w/"+slug)
	return send_from_directory("public/w", slug+".html"), 200
## create
@app.route("/w/create", methods=["POST", "GET"])
def app_w_create(): #TODO implement ceration of collections
	if request.method == "GET":
		return send_from_directory("public/w", "create.html")
	elif request.method == "POST":
		print(request.form)
		if request.form.get("creation-type") == "collection":
			print("create collection")
			abort(501)
		elif request.form.get("creation-type") == "page":
			print("create page")
			try:
				slug = ensure_slug(request.form.get("slug"))
				file_target = svc.a_add(slug, user="pegwiki")
				os.symlink(file_target, working_dir+"/"+"public/pages/"+slug+".text")

				svc.a_add(slug+".meta", user="pegwiki")
				render_simple_template("public/pages/"+slug+".meta", "public/templates/default.meta", {"slug":slug})

				render_simple_template("public/pages/"+slug+".html", "public/templates/default.html", {"slug":slug})
				return redirect(slug+"/edit")
			except (ValueError, AssertionError) as e:
				print(e)
				abort(400)
	else:
		abort(400)
		


## edit
@app.route("/<slug>/edit", methods=["GET"])
def app_edit_get(slug):
	print("app_edit_get: /"+slug)
	slug = ensure_slug(slug)
	meta = get_doc_meta(slug)
	form_id = random_identifier()
	try:
		app.config["previews"][slug].update({form_id: {"age": time.time()}})
	except KeyError:
		app.config["previews"][slug] = {form_id: {"age": time.time()}}
	return render_template("templates/edit.html", slug=slug,
	                                              form_id=form_id,
	                                              page_version=meta["version"],
	                                              title=meta["title"],
	                                              selected_content_type=app.config["inv-doc-types"][meta["Content-Type-plaintext"]],
	                                              content_types=app.config["list-doc-types"],
	                                              tags=meta.get("tags",[]),
	                                              page_text="pages/"+slug+".text",
	                                              preview_html="templates/empty"), 200


@app.route("/<slug>/edit", methods=["POST"])
def app_edit_post(slug):
	print("app_edit_post: /"+slug)
	#print(request.form)
	try:
		slug = ensure_slug(slug)
		meta = get_doc_meta(slug)
	except (ValueError, FileNotFoundError) as e:
		print("err: slug not valid, or doc does not exist.")
		abort(404)

	# validate form_id
	form_id = request.form.get("form_id")
	try:
		preview = app.config["previews"].get(slug).get(form_id)
		if time.time()-preview["age"] > app.config["form_id_max_age"]:
			return "form expired" #TODO issue warning to user that the form has expired
	except (TypeError, AttributeError) as e:
		print("err: no form_id or form_id not known to server.")
		abort(400)

	# validate document-type
	doc_type = app.config["doc-types"].get(request.form.get("content_type"))
	if not doc_type:
		print("err: doc_type not known to server") #TODO test
		abort(400)
	if doc_type == "text/latex; charset=utf-8":
		abort(501)
	# generate new metadata
	meta.update({"tags": request.form.get("tags").split(), #TODO validate, bleach tags and title
	             "title": request.form.get("title"), #TODO update user in meta
	             "Content-Type-plaintext": doc_type})

	# generate preview
	if request.form.get("preview"):
		# check version is current
		form_version = request.form.get("page_version")
		if form_version != str(meta["version"]):
			print("version mismatch. Provided: "+form_version+" latest: "+str(meta["version"]))
			fileHeader = "public/templates/markdown-header-preview-version-mismatch.html"
		else:
			fileHeader = "public/templates/markdown-header-preview.html"
		fileFooter = "public/templates/markdown-footer-preview.html"

		preview_name = slug+"."+form_id
		with open("public/previews/"+preview_name+".text", "w") as f:
			f.write(request.form.get("content"))
		with open("public/previews/"+preview_name+".meta", "w") as f:
			f.write(json.dumps(meta, indent=4, sort_keys=True))
		#meta.update({"raw_text": request.form.get("content")})
		md.parse_file(fileIn="public/previews/"+preview_name+".text",
		              fileOut="public/previews/"+preview_name+".html",
		              fileMeta=meta,
		              fileHeader=fileHeader,
		              fileFooter=fileFooter)

		return render_template("templates/edit.html", slug=slug,
		                                              form_id=form_id,
		                                              page_version=form_version, # !relies on being same as meta["version"]
		                                              title=meta["title"],
		                                              selected_content_type=request.form.get("content_type"),
		                                              content_types=app.config["list-doc-types"],
		                                              tags=meta.get("tags",[]),
		                                              page_text="previews/"+preview_name+".text",
		                                              preview_html="previews/"+preview_name+".html"
		                                              ), 200
		#return make_response(send_from_directory("public/previews/",preview_name+".html"))

	# generate page
	elif request.form.get("save"):
		print("save")

		# check version is current
		form_version = request.form.get("page_version")
		if form_version != str(meta["version"]):
			return "version mismatch. Provided: "+form_version+" latest: "+str(meta["version"])
			print("version mismatch. Provided: "+form_version+" latest: "+str(meta["version"]))
		meta["version"] += 1

		fileHeader = "public/templates/markdown-header.html"
		fileFooter = "public/templates/markdown-footer.html"

		new_file_bytes = request.form.get("content").encode()
		new_meta_bytes = json.dumps(meta, indent=4, sort_keys=True).encode()
		"""changed = svc.a_changed(file=slug, new_file_bytes=new_file_bytes)
		print("changed: "+str(changed))
		if changed:"""
		#TODO implement locking interface to prevent race conditions - check version again after acquiring a lock.
		# save file
		file_target = svc.a_commit(slug, new_file_bytes, "nick", "message")
		# save meta file
		meta_target = svc.a_commit(slug+".meta", new_meta_bytes, "nick", "message", unchanged_ok=True)
		# create new symlinks atomically 
		f_tmp = "public/temp/"+slug+".text"
		m_tmp = "public/temp/"+slug+".meta"
		f_lnk = "public/pages/"+slug+".text"
		m_lnk = "public/pages/"+slug+".meta"
		os.symlink(file_target, working_dir+"/"+f_tmp) #TODO prevent incoherent state of meta and text
		os.symlink(meta_target, working_dir+"/"+m_tmp)
		os.rename(working_dir+"/"+f_tmp, working_dir+"/"+f_lnk) #TODO lock tmp files to prevent overwriting
		os.rename(working_dir+"/"+m_tmp, working_dir+"/"+m_lnk) # maybe use hardlinks instead for performance?
		# render
		md.parse_file(fileIn=f_lnk, #TODO move old renders to .parsedhistory
		              fileOut="public/pages/"+slug+".html",
		              fileMeta=meta,
		              fileHeader=fileHeader,
		              fileFooter=fileFooter)

		response = redirect(slug)
		return response

	else:
		print("invalid submission type")
		abort(400) # not valid submission type
	return "200", 200

## serve
@app.route("/<slug>:text", methods=["GET"])
def app_get_plaintext(slug): #TODO x-accel the sending of pages
	try: 
		slug = ensure_slug(slug)
		meta = get_doc_meta(slug)
		version = request.args.get("v")
		print("slug: " + str(slug) + ", v=" + str(version))

		if version is None: # latest version if not otherwise specified
			response = make_response(send_from_directory("public/pages", str(slug)+".text"))
			response.headers["Content-Type"] = meta["Content-Type-plaintext"]
			return response, 200
		else:
			# does specified version identifier exist?
			print(meta["version"])
			if int(version) <= meta["version"]:
				response = make_response(send_from_directory("public/pages/.history", str(slug)+"/"+str(version)))
				response.headers["Content-Type"] = meta["Content-Type-plaintext"]
				return response, 200
			else:
				abort(404) # version doesn't exist
	except (FileNotFoundError, ValueError) as e:
		abort(404)

@app.route("/<slug>", methods=["GET"])
def app_get_page(slug): #TODO x-accell the sending of pages
	try: 
		slug = ensure_slug(slug)
		meta = get_doc_meta(slug)
		version = request.args.get("v")
		print("slug: " + str(slug) + ", v=" + str(version))

		if version is None: # latest version if not otherwise specified
			response = make_response(send_from_directory("public/pages", str(slug)+".html"))
			response.headers["Content-Type"] = meta["Content-Type"]
			return response, 200
		else:
			# does specified version identifier exist?
			if int(version) <= meta["version"]:
				response = make_response(send_from_directory("public/pages/.parsedhistory", str(slug)+"/"+str(version)))
				response.headers["Content-Type"] = meta["Content-Type"]
				return response, 200
			else:
				abort(404) # version doesn't exist
	except (ValueError, FileNotFoundError) as e:
		abort(404)

## serve static
@app.route("/", methods=["GET"]) 
def app_root():
	print("get root: index.html")
	return send_from_directory("public/w", "home.html"), 200

@app.route("/favicon.ico", methods=["GET"])
def app_favicon_serve():
	return send_from_directory("public/static", "favicon.ico"), 200

#TODO static route should be built in to flask - flask book chapter 3
@app.route("/static/<file>", methods=["GET"]) 
def app_static_serve(file):
	print("get static: " + str(file))
	return send_from_directory("public/static", file), 200

if __name__ == '__main__':
	# argv "setup=true"
	#    call setup() in setup.py
	#        check if instance already exists
	#        generate svc instance
	#        symlink folders
	#        generate index
	#        create sqlite db for users

	with open("secret", "r") as f:
		app.config["SECRET_KEY"] = f.readline().strip()
	print(app.url_map)
	app.run()


# either use js to save textarea for demo
# or if no js, let server send back the raw characters again