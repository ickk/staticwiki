#!/usr/bin/env python3
import sys, os

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'simplevc'))
from simplevc import SimpleVC

if __name__ == '__main__':
	working_dir = os.getcwd()
	svc = SimpleVC(working_dir)
	svc.initialise()

	os.makedirs("public/pages", exist_ok=True)
	os.makedirs("public/previews", exist_ok=True)
	os.makedirs("public/temp", exist_ok=True)
